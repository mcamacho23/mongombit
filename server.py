from flask import Flask, request, Response
import json
import datetime
from bson.objectid import ObjectId


import mongo

app = Flask(__name__)


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime.datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)


def ret(value):
    return Response(JSONEncoder().encode(value), mimetype='application/json')

@app.route("/")
def provision():
    return mongo.provision_db()


@app.route("/list")
def list_content():
    ret_val = mongo.list_content(request.values)
    return ret(ret_val)


@app.route("/plan")
def plan():
    ret_val = mongo.plan(request.values)
    return ret(ret_val)

@app.route("/by_taxonomy")
def by_taxonomy():
    ret_val = mongo.by_taxonomy(request.values)
    return ret(ret_val)


if __name__ == "__main__":
    app.run()
