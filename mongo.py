import datetime
from pymongo import MongoClient  # Database connector
from data import data


client = MongoClient('localhost', 27017)      # Configure the connection to the database
db = client.mydatabase                          # Select the database
mycollection = db.mycollection                  # Select the collection


def iter_cursor(cursor):
    return [doc for doc in cursor]


def provision_db():
    mycollection.delete_many({})
    mycollection.insert_many(data)


def list_content(request_values):
    page_size = int(request_values.get('page_size', 10))
    page = int(request_values.get('page', 1))
    skips = page_size * (page - 1)
    cursor = mycollection.find().skip(skips).limit(page_size)
    return iter_cursor(cursor)


def plan(request_values):
    starts = request_values.get('starts', "23-01-2019")
    ends = request_values.get('ends', "23-02-2019")
    cursor = mycollection.find({"$and": [{'starts': {'$gte': datetime.datetime.strptime(starts, '%d-%m-%Y')}},
                                         {'ends': {'$lte': datetime.datetime.strptime(ends, '%d-%m-%Y')}}],
                                }).sort([('recommendation_weight', -1)])
    return iter_cursor(cursor)


def by_taxonomy(request_values):
    taxonomy = request_values.get('taxonomy')
    cursor = mycollection.find({'taxonomies.slug': taxonomy}).sort([('recommendation_weight', -1)])
    return iter_cursor(cursor)
